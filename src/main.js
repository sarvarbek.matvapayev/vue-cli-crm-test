import {
  createApp,
  VueElement
} from 'vue'
import App from './App.vue'
import './registerServiceWorker'
import router from './router'
import store from './store'
import 'materialize-css/dist/js/materialize.min.js'
import 'materialize-css/dist/css/materialize.min.css';
import './assets/index.css';
import Vue from 'vue'
import messagePlugin from '@/utils/message.plugin'
import Loader from '@/components/app/Loader'
import tooltipDirective from '@/directives/tooltip.directive'
import Paginate from 'vuejs-paginate-next'



// Import the functions you need from the SDKs you need
import {initializeApp} from "firebase/app";
import {getAuth, onAuthStateChanged } from 'firebase/auth'
import 'firebase/database'

// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
// For Firebase JS SDK v7.20.0 and later, measurementId is optional
const firebaseConfig = {
  apiKey: "AIzaSyBa1wSd59PRYA8jswUOC4cCyLCj8q8DRGU",
  authDomain: "vue-cli-crm-test.firebaseapp.com",
  projectId: "vue-cli-crm-test",
  storageBucket: "vue-cli-crm-test.appspot.com",
  messagingSenderId: "165554942818",
  appId: "1:165554942818:web:de0ebd2dc93be1e7e6f23b",
  measurementId: "G-H5NS7NZN6Y"
};

let app
// Initialize Firebase
initializeApp(firebaseConfig);
const auth=getAuth()
onAuthStateChanged(auth, (user) => {
  if (!app) {
    app = createApp(App)
    app.use(store)
    app.use(router)
    app.use(messagePlugin)
    app.directive('tooltip', (el, binding)=> {
      M.Tooltip.init(el, {html: binding.value});

    })
    app.component('Loader',Loader);
    app.component('Paginate',Paginate);
    app.mount('#app')
  }
})

//createApp(App).use(store).use(router).use(messagePlugin).mount('#app')
