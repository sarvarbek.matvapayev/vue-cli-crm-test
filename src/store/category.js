import firebase from 'firebase/app'

export default{
    actions:{
        async fetchCategories({commit, dispatch}){
            try{
                const uid=await dispatch('getUid');
                // const categories= (await firebase.database().ref(`/users/${uid}/categories`).once('value')).val() || {};
                const categories={};
                categories["key1"]={title: "Youtube", limit: 10000};
                categories["key2"]={title: "Home", limit: 5000};
                categories["key3"]={title: "Girls", limit: 2000};

                /**
                 * Method 1
                 */
                // const cats=[];
                // Object.keys(categories).forEach(key=> {
                //     cats.push({
                //         title: categories[key].title,
                //         limit: categories[key].limit,
                //         id: key,
                //     });
                // });
                // return cats;

                /**
                 * Method 2
                 */
                return Object.keys(categories).map(key=>({...categories[key], id:key}));
            }
            catch (e) {
                commit('setError',e)
                throw e;
            }
        },
        async createCategory({commit, dispatch},{title, limit}) {
            try{
                const uid=await dispatch('getUid');
                // const category=await firebase.database().ref(`/users/${uid}/categories`).push({title, limit});
                // return {title, limit, uid: category.key }

                const category={title: title, limit: limit, uid:Math.random().toString(36).slice(2)};
                return category;
            }
            catch (e) {
                commit('setError',e)
                throw e;
            }
        },
        async updateCategory({commit, dispatch},{title, limit, id}){
            try{
                const uid=await dispatch('getUid');
                // await firebase.database().ref(`/users/${uid}/categories`).child(id).update({title, limit});
                const category={title: title, limit: limit, uid:id};
                return category;
            }
            catch (e) {
                commit('setError',e)
                throw e;
            }
        },
        async fetchCategoryById({commit, dispatch}, id){
            try{
                const categories=await dispatch('fetchCategories');
                const category=categories.find(c=>c.id===id);
                return category;
            }
            catch (e) {
                commit('setError',e)
                throw e;
            }
        }
    }
}