import { createStore } from 'vuex'
import auth from './auth'
import info from './info'
import category from './category'
import record from './record'
export default createStore({
  state: {
    error: null,
  },
  actions:{
    async fetchCurrency(){
      const key=process.env.VUE_APP_FIXER;
      const headers={'apikey':key }
      const res=await fetch(`https://api.apilayer.com/fixer/latest?base=USD&symbols=EUR,USD,RUB`,{headers:headers});
      return await res.json();
    }
  },
  getters: {
    error: s=>s.error,
  },
  mutations: {
    setError(state, error){
      state.error = error
    },
    clearError(state, error){
      state.error = null
    }
  },
  modules: {
    auth, info, category, record
  }
})
