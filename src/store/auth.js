import { getAuth, signInWithEmailAndPassword, signOut, createUserWithEmailAndPassword  } from "firebase/auth";

export default {
  actions: {
    async login({
      dispatch,
      commit
    }, {
      email,
      password
    }) {
      try {
        const auth = getAuth();
        await signInWithEmailAndPassword(auth, email, password)
      } catch (e) {
        commit('setError', e);
        throw e
      }
    },
    async logout({commit}){
        const auth = getAuth();
        await signOut(auth);
        commit('clearinfo');
    },
    async register({dispatch, commit}, { email, password, name}) {
        try {
            const auth = getAuth();
            await createUserWithEmailAndPassword(auth, email, password)
            const uid=await dispatch('getuid');
            await firebase.database().ref(`/users/${uid}/info`).set({
              bill:10000,
              name: name
            })
          } catch (e) {
            commit('setError', e);
            throw e
          }
    },
    getUid(){
        // const auth = getAuth();
        // const user = auth.currentUser;
        // return user ? user.uid :null;

        const uid=Math.random().toString(36).slice(2);
        return uid;
    }
  }
}
