import firebase from 'firebase/app'

export default {
    actions: {
        async createRecord({dispatch, commit}, record){
            try {
                const uid=await dispatch('getUid');
                await firebase.database().ref(`/users/${uid}/records`).push(record);

            }catch(e){
                commit('setError',e);
                throw e;
            }
        },
        async fetchRecords({dispatch, commit}){
            try{
                const record1={
                    id:"key1",
                    type:'income',
                    categoryId:"key1",
                    amount:1000,
                    date:new Date().toJSON(),
                };

                const record2={
                    id:"key2",
                    type:'outcome',
                    categoryId:"key2",
                    amount:1000,
                    date:new Date().toJSON(),
                };

                const records=[];
                records.push(record1);
                records.push(record2);

                return records;

                // const uid=await dispatch('getUid');
                // const records=(await firebase.database().ref(`/users/${uid}/records`).once('value')).val() || {};
                // return Object.keys(records).map(key=> ({...records[key], id:key}))

            }catch(e){
                commit('setError',e);
                throw e
            }
        },
        async fetchRecordById({dispatch, commit}, id){
            try{
                const records=await dispatch('fetchRecords');
                const record=records.find(c=>c.id===id);
                return {...record}
            }catch(e){
                commit('setError',e);
                throw e
            }
        }
    }
}