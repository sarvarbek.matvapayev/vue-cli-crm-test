import firebase from 'firebase/app'

export default{
    state:{
        info:{}
    },
    mutations:{
        setInfo(state, info){
            state.info =info;
        },
        clearInfo(state){
            state.info ={}
        }
    },
    actions:{
        async fetchinfo({dispatch, commit}){
            try{
                const info={'name':"Sarvarbek",'bill':10000, 'locale':"ru-RU2"};
            //     const uid=await dispacth('getUid');
            // const info=(await firebase.database.ref(`/users/${uid}/info`).once('value')).val()
            commit('setInfo',info)
            }
            catch(e){
                commit('setError',e);
                throw e;
            }

        },
        async updateInfo({commit, dispatch}, toUpdate){
            try{
                const updatedata= {...getters.info, ...toUpdate}
                // const uid=await dispacth('getUid');
                // const info=(await firebase.database.ref(`/users/${uid}/info`).update(toUpdate));

                commit('setInfo', updatedata)
            }
            catch(e){
                commit('setError',e);
                throw e;
            }
        },
    },
    getters:{
        info: s=>s.info
    }
}
